import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/layout.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    redirect: 'home',
    meta: { title: '首页' },
    children: [
      {
        path: "home", component: () => import("@/views/test1.vue"),
        meta: { title: '首页' },
      },
    ],
    hidden: true
  },
  {
    path: '/about',
    name: 'about',
    meta: { title: '可视化大屏' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/test2',
    component: HomeView,
    meta: { title: '用户管理' },
    redirect: 'test2-1',
    children: [
      {
        path: 'test2-1',
        component: () => import('@/views/test2.vue'),
        meta: { title: '用户菜单' },
      }
    ]
  },
  {
    path: '/test3',
    component: HomeView,
    redirect: 'test3-1',
    meta: { title: '设置' },
    children: [
      {
        path: 'test3-1',
        component: () => import('@/views/test3.vue'),
        meta: { title: '用户设置' },
      }
    ]
  },
  {
    path: '/test4',
    component: HomeView,
    redirect: '/shop/list',
    hidden: true,
    meta: { title: '店铺管理' },
    children: [
      {
        path: '',
        component: () => import('@/views/test4.vue'),
        meta: { title: '店铺管理' },

      }
    ]
  },
]
sessionStorage.setItem('sell_menus', JSON.stringify(routes))

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
